#include "AnimationWindow.h"
#include <iostream>

int main()
{
	TDT4102::AnimationWindow window;
	
	// Message box
	window.show_message("test title", "Message", TDT4102::MessageType::INFO);
	window.show_message("test title", "Message", TDT4102::MessageType::ERROR);
	window.show_message("test title", "Message", TDT4102::MessageType::QUESTION);
	window.show_message("test title", "Message", TDT4102::MessageType::WARNING);

	// Yes no question dialog
    bool result = window.show_yesno_dialog("test title", "message question", TDT4102::MessageType::QUESTION);
	result = window.show_yesno_dialog("test title", "message question", TDT4102::MessageType::ERROR);
	result = window.show_yesno_dialog("test title", "message question", TDT4102::MessageType::INFO);
	result = window.show_yesno_dialog("test title", "message question", TDT4102::MessageType::WARNING);

	if(result == true) {
		std::cout << "You answered yes" << std::endl;
	} else {
		std::cout << "You answered no" << std::endl;
	}

	// System notifications
    window.show_notification("test notification", "test notification message", TDT4102::MessageType::INFO);
	window.show_notification("test notification", "test notification message", TDT4102::MessageType::ERROR);
	window.show_notification("test notification", "test notification message", TDT4102::MessageType::WARNING);
	window.show_notification("test notification", "test notification message", TDT4102::MessageType::QUESTION);

	// File open and saving dialogs
    std::filesystem::path fileToOpen = window.show_open_file_dialog("File to open");
    std::filesystem::path directoryToOpen = window.show_select_directory_dialog("Directory to open");
    std::filesystem::path fileToSave = window.show_save_file_dialog("File to save");

	window.wait_for_close();
	return 0;
}