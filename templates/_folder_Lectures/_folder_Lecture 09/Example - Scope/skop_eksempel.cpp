#include <vector>
#include <iostream>

//------------------------------------------------------------------------------'
// Eksempel fra gjesteforelesning 18.03.25
// Skop

double finnRabatt(double p) {
    return 0.1 * p;
}

int main() {
    std::vector<double> priser {50, 10};
    double totalRabatt = 0;

    for (int i = 0; i < priser.size(); i++) {
        double pris = priser.at(i); 

        if (pris > 20) {
            totalRabatt += finnRabatt(pris); 
        }
    }

    std::string melding = "Rabatt på varen er: ";

    if (priser.size() > 1){
        std::string melding = "Rabatt på varene er: ";
        std::cout << "Vi har flere varer." << std::endl;
        std::cout << melding << std::endl;
    }

    std::cout << melding << totalRabatt << std::endl;
    return 0;
}

//------------------------------------------------------------------------------
