// Bug: The functions in this example contain a bug. If a and b are equal, the function exits without encountering a return statement.
// This results in undefined behaviour; that is, the C++ standard does not specify what should happen in that circumstance, and it is
// up to the compiler to decide what it wants to do with it.
// Some compilers may warn you about this bug. Keep an eye out while the program is being compiled! =)

#include "std_lib_facilities.h"

int maxOfTwo(int a, int b) {
    if (a > b) {
        return a;
    } else if (b > a) {
        return b;
    }
}

double maxOfTwo(double a, double b) {
    if (a > b) {
        return a;
    } else if (b > a) {
        return b;
    }
}

int main() {
    try {
        // We start by testing the function with parameters of type double.
        // In this first case, 3 is bigger than 2, so we expect that to be the value returned by the function.
        cout << "Test 1 - Expected: 3, Got: " << maxOfTwo(2.0, 3.0) << endl;

        // Let's now try passing in two equal numbers:
        cout << "Test 2 - Expected: 2, Got: " << maxOfTwo(2.0, 2.0) << endl;

        // Oh dear, that did not go well. What if we pass in a number that's almost equal to 2?
        cout << "Test 3 - Expected: 2, Got: " << maxOfTwo(2.0, 1.99999999) << endl;

        // In that case it worked fine. However, once we add more digits, the number will be ever closer to 2.0.
        // Values of type double have a limited number of bits to work with, so eventually the compiler needs to round to the next value it can represent with the bits it has
        // So, if we add yet more digits, the compiler will replace the second parameter with the value of 2.0, which is closest to the one we specified.
        // As you will see here, the problem occurs again.
        cout << "Test 4 - Expected: 2, Got: " << maxOfTwo(2.0, 1.9999999999999999) << endl;

        // Of course this problem is not limited to functions taking doubles.
        // Any function which in some cases does not encounter a return statement (unless its return type is void of course) can have this problem.
        // Here's the same function using integer parameters, which has the exact same problem:
        cout << "Test 5 - Expected: 5, Got: " << maxOfTwo(4, 5) << endl;
        cout << "Test 6 - Expected: 4, Got: " << maxOfTwo(4, 4) << endl;

    } catch (exception& e) {
        cout << "Exception caught: " << e.what() << endl;
        // An error occurred, so we return a nonzero exit code to the operating system to let it know something went wrong
        return -1;
    }
}