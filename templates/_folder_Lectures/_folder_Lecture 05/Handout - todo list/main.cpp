#include <iostream>

int main() {
    std::string todoList = "buy soap\nbuy rice\ndo laundry\naccidentally publish exam on blackboard\ninvent mind control device\nprepare world domination victory speech\ncall mom\n";
    std::string input;
    while(true) {
        std::cout << "? ";
        getline(std::cin, input);
        if(input == "quit") {
            break;
        }
        todoList += input + '\n';
        std::cout << "--- TODO list ---" << std::endl;
        std::cout << todoList << std::endl;
    }

    return 0;
}