#include "Stopwatch.h"
#include "measurePerformance.h"
#include "optimizeVector.h"
#include "optimizationTask.h"
#include "templatefunctions.h"
#include "MyArray.h"



int main() {
    // Oppgave 1
    //measurePerformanceUnique();
    //measurePerformanceShared();
    //measurePerformanceStack();
    //measurePerformanceHeap();

    // Oppgave 2
    //optimizeVector();

    // Oppgave 3
    //optimizationTask();

    // Oppgave 4
    //testTemplateFunctions();
    
    // Oppgave 5
    //testMyArray();
    return 0;
}