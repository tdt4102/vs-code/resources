#pragma once
#include "std_lib_facilities.h"

constexpr double pi = 3.14159265;
constexpr double gravity = 9.81;

// Del 1:

// BEGIN: 1a
// Deklarer funksjonen acclY 
// END: 1a

// BEGIN: 1b
// Deklarer funksjonen velY
// END: 1b

// BEGIN: 1c
// Deklarer funksjonene posX og posY
// END: 1c

// BEGIN: 1d
// Deklarer funksjonen printTime
// END: 1d

// BEGIN: 1e
// Deklarer funksjonen flightTime
// END: 1e

bool testDeviation(double compareOperand, double toOperand, double maxError, string name);

// Del 2:
// BEGIN: 4a
// Her skal du deklarere ALLE funksjonene i oppgave 4a
// END: 4a

// BEGIN: 4b

// END: 4b

// BEGIN: 4c

// END: 4c

// Del 3:

// BEGIN: 5b
// Deklarer funksjonen playTargetPractice her
// END: 5b
