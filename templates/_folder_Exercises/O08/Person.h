#pragma once
#include "Car.h"
#include <string>
#include <iostream>
#include <memory>

class Person
{
public:
	// BEGIN 2b

	// END 2b

	// BEGIN 2c

	// END 2c

	// BEGIN 2d

	// END 2d

	// Vi onsker ikke å tillate kopiering av Person-objekter
	Person(const Person&) = delete;
	Person& operator=(const Person&) = delete;

private:
	// BEGIN 2a

	// END 2a
};
