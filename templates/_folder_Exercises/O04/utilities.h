
#pragma once
#include "std_lib_facilities.h"

// BEGIN: 1b
 // deklarerer funksjonen incrementByValueNumTimes her
// END: 1b

// BEGIN: 1d
 // deklarerer funksjonen incrementByValueNumTimesRef her
// END: 1d

// BEGIN: 1e
 // deklarerer funksjonen swapNumbers her
// END: 1e

// BEGIN: 2a
 // lag struct Student her
// END: 2a

// BEGIN: 2b
// deklarerer funksjonen printStudent her
// END: 2b

// BEGIN: 2c
// deklarerer funksjonen isInProgram her
// END: 2c

// BEGIN: 3a
 // deklarerer funksjonen randomizeString her
// END: 3a

// BEGIN: 3c
 // deklarerer funksjonen readInputToString her
// END: 3c

// BEGIN: 3d
 // deklarerer funksjonen countChar her
// END: 3d
