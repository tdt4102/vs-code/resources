#include <iostream>
#include <chrono>
#include <vector>
#include "Stopwatch.h"

int main() {
    Stopwatch watch;
    watch.start();

    constexpr int gridSize = 25000;

    std::vector<std::array<int, gridSize>> grid(gridSize);

    long long sum = 0;
    for(int row = 0; row < gridSize; row++) {
        for(int col = 0; col < gridSize; col++) {
            //std::string hello = "Hello there!";
            sum += grid.at(row).at(col);
        }
    }

    std::cout << "Time taken: " << watch.stop() << " seconds" << std::endl;

    return 0;
}