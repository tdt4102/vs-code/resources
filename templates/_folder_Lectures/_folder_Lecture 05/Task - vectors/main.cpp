#include <iostream>
#include <vector>
#include <random>

// Declarations for a couple of utility functions
void printVector(std::vector<int> listOfNumbers);
std::vector<int> generateRandomNumberVector();

int main() {
    std::vector<int> listOfNumbers = generateRandomNumberVector();
    std::cout << "Initial list of numbers: ";
    printVector(listOfNumbers);

    // TASK: listOfNumbers contains a vector of a random length with
    // random numbers inside. Write a few lines of code below this comment
    // that reverses the contents of the vector
    // Example input: [15, 16, 18, 13, 4, 14]
    // Example output: [14, 4, 13, 18, 16, 15]


    std::cout << "Reversed list of numbers: ";
    printVector(listOfNumbers);

    return 0;
}


// --- Utility functions ---
// You can snoop around a bit here, but it should not be necessary to complete this task
// I've tried to explain that stuff does with comments :)

void printVector(std::vector<int> listOfNumbers) {
    std::cout << "[";
    // This loop iterates through each element in listOfNumbers
    for(int i = 0; i < listOfNumbers.size(); i++) {
        std::cout << listOfNumbers.at(i);
        // We don't print a , for the final number, as it doesn't look pretty
        if(i != listOfNumbers.size() - 1) {
            std::cout << ", ";
        }
    }
    std::cout << "]" << std::endl;
}

std::vector<int> generateRandomNumberVector() {
    // Generates a vector with random numbers of a random length
    // The vector will be between 0 and 20 elements long, 
    // and each number in the vector will also be in that range.
    std::random_device device;
    std::default_random_engine engine(device());
    std::uniform_int_distribution distribution(0, 20);

    std::vector<int> list;
    int howManyNumbersToGenerate = distribution(engine);
    for(int i = 0; i < howManyNumbersToGenerate; i++) {
        list.push_back(distribution(engine));
    }
    return list;
}