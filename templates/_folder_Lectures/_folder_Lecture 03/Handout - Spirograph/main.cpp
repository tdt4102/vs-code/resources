#include "std_lib_facilities.h"
#include "AnimationWindow.h"

double spirographX(double radius1, double radius2, double multiplier, double t) {
    return (radius1 - radius2) * std::cos(t) + multiplier * std::cos(((radius1 - radius2) / radius2) * t);
}

double spirographY(double radius1, double radius2, double multiplier, double t) {
    return (radius1 - radius2) * std::sin(t) + multiplier * std::sin(((radius1 - radius2) / radius2) * t);
}

int main() {
    
        
    return 0;
}