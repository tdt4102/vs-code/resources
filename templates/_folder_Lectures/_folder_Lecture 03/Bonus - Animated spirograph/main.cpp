#include "std_lib_facilities.h"
#include "AnimationWindow.h"

double spirographX(double radius1, double radius2, double multiplier, double t) {
    return (radius1 - radius2) * std::cos(t) + multiplier * std::cos(((radius1 - radius2) / radius2) * t);
}

double spirographY(double radius1, double radius2, double multiplier, double t) {
    return (radius1 - radius2) * std::sin(t) + multiplier * std::sin(((radius1 - radius2) / radius2) * t);
}

int main() {
    // Opens a new window (no black magic involved!)
    AnimationWindow window;

    // Tweak these numbers to change the shape of the spyrograph
    // For best results, make sure that 
    const double radius1 = 328;
    const double radius2 = 600;
    const double multiplier = 94;

    // Higher number = faster drawing, easier for slower computers
    // Lower number = slower drawing but more accurate
    const double lengthPerStep = 0.1;

    // Increase if drawing stops too early
    const double lengthToDraw = 500;

    // Allows you to move the drawn image
    const Point centreCoordinate = {400, 400};

    for(double drawUpToLength = 0; drawUpToLength < lengthToDraw; drawUpToLength += lengthPerStep) {
        // Uncomment to show the length of the drawing
        // window.draw_text({30, 30}, std::to_string(drawUpToLength), Color::black);
        
        // This creates a new animation frame
        window.next_frame();
        
        // Draws the spyrograph curve up to drawUpToLength
        for(double t = 0.0; t < drawUpToLength; t += lengthPerStep) {
            Point currentPoint;
            currentPoint.x = centreCoordinate.x + spirographX(radius1, radius2, multiplier, t);
            currentPoint.y = centreCoordinate.y + spirographY(radius1, radius2, multiplier, t);
            Point nextPoint;
            nextPoint.x = centreCoordinate.x + spirographX(radius1, radius2, multiplier, t + lengthPerStep);
            nextPoint.y = centreCoordinate.y + spirographY(radius1, radius2, multiplier, t + lengthPerStep);
            
            window.draw_line(currentPoint, nextPoint);
        }
    }

    // After we have drawn the final image of the animation, wait for the window to be closed
    window.wait_for_close();
    
    return 0;
}