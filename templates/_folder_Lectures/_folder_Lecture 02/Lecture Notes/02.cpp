// Show: compile a project with norwegian characters on Windows

// - Repetition from last time:
//     - Configuration using Meson
//     - Compilation, compiler errors, compiler warnings
//     - The main function
//     - cout, endl, strings
//     - Include means import
//     - Debugging
//     - You can also print numbers with cout

// -     ^ Ensure the final example you show prints a number

// - We can print numbers, but we also want to do stuff with them
//   -> we need variables

// - Show:
int value = 5;
//   A variable consists of two parts: a datatype and a name, which can be initialised to a value.
//     - Like Python, each variable has a data type
//     - Unlike Python, the type is not automatically deduced

// - Show:
int value = 5;
cout << value << endl;
//   Prints out 5!

// - Show:
int value = 5;
cout << "The value of value is: " << value << endl;
//   We can mix and match different values!
//   .. As long as they are separated using <<
//   Note: it's like typing on a keyboard, so you need to have a space at the end

// - Show:
int value;
cout << "The value of value is: " << value << endl;
// SHOW COMPILER WARNING
//   Unlike Python, we can create a variable without assigning a value to it
//   Run the program, show that the number is kind of random (seems to be 0 on windows)

// - Good practice: always assign a value when you declare a variable!

// SHOW SUMMARY SLIDES FOR VARIABLES

// Float: 1-8-23
// Double: 1-11-52

// COME BACK AFTER QUESTION 2 IN DATA TYPES

// Objective: show integer overflows and underflows
// - Clear out main()
// - Write new lines:
int value = 0;
cout << "Before: " << value << endl;
value = value - 1;
cout << "After: " << value << endl;
// - Run program, output is -1

// - Change to:
unsigned int value = 0;
cout << "Before: " << value << endl;
value = value - 1;
cout << "After: " << value << endl;
// - Run program, output is 4294967295
// - Note: not even a compiler warning!

// - Change to:
unsigned int value = 4294967295;
cout << "Before: " << value << endl;
value = value + 1;
cout << "After: " << value << endl;
// - Run program, output is 0

// - Change to:
int value = 2147483647;
cout << "Before: " << value << endl;
value = value + 1;
cout << "After: " << value << endl;
// - Run program, output is -2147483648

// - Change to:
int value = -2147483648;
cout << "Before: " << value << endl;
value = value - 1;
cout << "After: " << value << endl;
// - Run program, output is 2147483647

// Demonstration: Minecraft far lands
// Version: beta 1.7.3
// Reason: perlin noise at scale 171.103 causes overflow because:
//         2,147,483,647 / 171,103 = 12,550,224
// /tp @p 12550700 90 0 in version 1.12
// use seed FAR LANDS
// You should show two things:
// - The terrain generator going haywire
// - The movement is choppy in one direction

// RETURN HERE AFTER SHOWING INTRO SLIDE ON TYPE CASTING

// We can demonstrate the minecraft problem here

// Show:
double doubleValue = 12550224.8;
float floatValue = doubleValue;
double convertedBack = floatValue;
// ! SHOW WARNING GENERATED
// ! USE DEBUGGER TO SHOW THIS

// Show: Conversion can result in weird behaviour when the type is too small
int intValue = 500;
char charValue = intValue;
int convertedBack = charValue;
cout << "The 500 ended up being: " << convertedBack << endl;
// prints -12
// Conversion only makes sense if the value "fits" in the type

// Show: This is how we cast, but it does not change how the conversion is done
int intValue = 500;
char charValue = static_cast<char>(intValue);
int convertedBack = charValue;
cout << "The 500 ended up being: " << convertedBack << endl;
// Compiler warning is also gone now!

// Show: alternate form for basic types
int intValue = 500;
char charValue = char(intValue);
int convertedBack = charValue;
cout << "The 500 ended up being: " << convertedBack << endl;

// Show: calculation is incorrect
int a = 5;
int b = 2;
double doubleValue = a / b;
cout << "The computed value is: " << doubleValue << endl;

// Show: Now it is correct!
int a = 5;
int b = 2;
double doubleValue = double(a) / double(b);
cout << "The computed value is: " << doubleValue << endl;

// Also possible to convert one value to a double by writing it as such


// NEXT TOPIC: OPERATORS, show intro slide for that then come back

// Show: numbers being multiplied
int eightTimesSeven = 8 * 7;
cout << "Result: " << eightTimesSeven << endl;

// Show: can also use variables as operands
int eight = 8;
int seven = 7;
int eightTimesSeven = eight * seven;
cout << "Result: " << eightTimesSeven << endl;

// Show: try division. Explain integer division afterwards
int eight = 8;
int seven = 7;
int eightDividedBySeven = eight / seven;
cout << "Result: " << eightDividedBySeven << endl;

// Show: the integer division operator of Python does not exist in C++
int eight = 8;
int seven = 7;
int eightDividedBySeven = eight // seven;
cout << "Result: " << eightDividedBySeven << endl;

// Show: remainder using modulo
int eight = 8;
int seven = 7;
int eightModuloSeven = eight % seven;
cout << "Result: " << eightModuloSeven << endl;

// Show: forcing precedence using ()
int value = (4 + 2) * (7 - 3);
cout << "Result: " << value << endl;

// Show: += operator
// Note that this _UPDATES_ the value!
int value = (4 + 2) * (7 - 3);
value += 4;
cout << "Result: " << value << endl;

// Show: ++ operator
int value = (4 + 2) * (7 - 3);
value++;
++value;
cout << "Result: " << value << endl;

// Show: what happens when you divide an integer by zero
//       If detectable by the compiler: warning
//       At runtime: exception thrown

// Show: what happens when you divide a float by zero
//       If detectable by the compiler: warning
//       At runtime: answer is infinity

// That's it for the algebraic operators

// Logic operators compare values, and result in a boolean value

// Show: booleans are printed as 0 and 1
cout << "False is: " << false << endl;
cout << "True is: " << true << endl;

// Show (maybe leave the above two lines):
bool areEqual = 5 == 4;
bool areNotEqual = 5 != 4;
cout << "5 == 4: " << areEqual << endl;
cout << "5 != 4: " << areNotEqual << endl;

// Show (on the blackboard):
// A B   A and B      A B   A OR B
// F F   F            F F   F
// F T   F            F T   T
// T F   F            T F   T
// T T   T            T T   T

cout << areEqual || areNotEqual << endl;
// Gives an error. Surround with () to resolve

cout << (areEqual && areNotEqual) << endl;

// When is it used? Example (chess):
bool checkmate = kingChecked && kingCannotEscape;

bool isVictory = checkmate || opponentResigned;

// SHOW SUMMARY SLIDES

// TEXT INPUT USING CIN
// Show intro slide first

// Show: basic usage
string message;
cout << "Write a message: ";
cin >> message;
cout << "The message was: " << message << endl;

// Show: enter one word
// Show: enter a bunch of spaces, then one word
// Show: enter a bunch of spaces and blank lines, then one word
// Show: enter two words

// Show: reading a double
double value;
cout << "Write a number: ";
cin >> value;
cout << "Value: " << value << endl;

// Show: entering multiple numbers only prints the first one

// Show: entering multiple numbers will cause each cin to grab one number each
// Show: entering three numbers will cause the program to wait for number 4
double value;
cout << "Write some numbers: ";
cin >> value;
cout << "Value 1: " << value << endl;
cin >> value;
cout << "Value 2: " << value << endl;
cin >> value;
cout << "Value 3: " << value << endl;
cin >> value;
cout << "Value 4: " << value << endl;

// Show: What if we want to read a message as-is?
string message;
cout << "Write a message: ";
getline(cin, message);
cout << "The message was: " << message << endl;

// NEXT TOPIC: IF STATEMENTS

// What do we mostly use booleans for?

// Show: basic if statement, show expression explicitly
int value;
cout << "Enter a number: ";
cin >> value;
bool isSmallerThan60 = value < 60;
if(isSmallerThan60) {
	cout << value << " is smaller than 60" << endl;
}

// Show: there's something missing
int value;
cout << "Enter a number: ";
cin >> value;
bool isSmallerThan60 = value < 60;
if(isSmallerThan60) {
	cout << value << " is smaller than 60" << endl;
} else {
	cout << value << " is greater than 60" << endl;
}

// Show: we need another case
int value;
cout << "Enter a number: ";
cin >> value;
if(value < 60) {
	cout << value << " is smaller than 60" << endl;
} else if(value > 60) {
	cout << value << " is greater than 60" << endl;
} else {
	cout << value << " is exactly 60" << endl;
}

// SHOW SUMMARY SLIDES

// NEXT TOPIC: LOOPS

// Show:
while(true) {
	cout << "lo";
}

// Show:
int sum = 0;
while(true) {
	sum++;
	cout << sum << " ";
}

// Show: something more useful
int sum = 10000;
while(sum > 0) {
	sum--;
	cout << sum << " ";
}

// Show: USE DEBUGGER
int sum = 10;
while(sum > 0) {
	sum--;
	cout << sum << endl;
}

// Show: start with python for loop, convert to C++ version
for i in range(0, 10):
	print(i)

// becomes:
for(int i = 0; i < 10; i++) {
	cout << i << endl;
}
// USE DEBUGGER!

// Show WITH DEBUGGER:
int i = 0;
do {
	i--;
} while(i > 0);
cout << "The value of i is: " << i << endl;

// Show WITH DEBUGGER:
int i = 1;
do {
	i--;
} while(i > 0);
cout << "The value of i is: " << i << endl;

// NEXT TOPIC: FUNCTIONS

// Show:
void sayHello() {
	cout << "Hello!" << endl;
}

// Show: basic syntax
bool isGreater(int a, int b) {
	return a > b;
}

// Show: using a return value that is missing
bool isGreater(int a, int b) {}

int main() {
	cout << "5 > 2: " << isGreater(5, 2) << endl;
}

// Show: use function as expression
// Show: declaration versus definition


// Finally: develop a small calculator?