#include "std_lib_facilities.h"

void functionA() {
	cout << "I am function A" << endl;
}

void functionB();

int main() {
	functionA();
	functionB();
	return 0;
}

void functionB() {
	cout << "I am function B" << endl;
}