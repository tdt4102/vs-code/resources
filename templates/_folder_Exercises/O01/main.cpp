// Øving 1
// Oversettelse fra Python til C++
#include "std_lib_facilities.h"

int maxOfTwo(int a, int b)   {
    // BEGIN: 2a
	
    // END: 2a
}

int fibonacci(int n) {
    // BEGIN: 2c

    // END: 2c
}

int squareNumberSum(int number) {
    // BEGIN: 2d

    // END: 2d
}

void triangleNumbersBelow(int number) {
    // BEGIN: 2e

    // END: 2e
}

bool isPrime(int number) {
    // BEGIN: 2f

    // END: 2f
}

void naivePrimeNumberSearch(int maxNumber) {
    // BEGIN: 2g

    // END: 2g 
}

void inputAndPrintNameAndAge() {
    // BEGIN: 2h

    // END: 2h
}

int main() {
    // BEGIN: 2b

    // END: 2b
	return 0;
}
