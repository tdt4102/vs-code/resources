param(
    [Parameter()]
    [String]$absolute_path
)
$absolute_path

# Check if it already is on path
$oldpath = [Environment]::GetEnvironmentVariable("Path", [EnvironmentVariableTarget]::User)
$listy = $oldpath -split ";"
$is_on_path_already = $listy.Contains($absolute_path)

# Add to path
if (! $is_on_path_already) {
    [Environment]::SetEnvironmentVariable(
        "Path",
        [Environment]::GetEnvironmentVariable("Path", [EnvironmentVariableTarget]::User) + ";"+ $absolute_path+ ";",
        [EnvironmentVariableTarget]::User)
    Write-Host "Added" $absolute_path "to PATH"
} else {
    Write-Host $absolute_path "is already on PATH"
}