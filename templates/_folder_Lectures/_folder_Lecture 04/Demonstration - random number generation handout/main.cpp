#include <iostream>

int main() {
    int luckyNumber = 0;
    std::cout << "Please enter your lucky number: ";
    std::cin >> luckyNumber;

    int winningNumber = 0;

    std::cout << "The winning number is.." << std::endl;
    std::cout << "Wait for it.." << std::endl;
    std::cout << "Building suspense.." << std::endl;
    std::cout << ">> " << winningNumber << " <<" << std::endl << std::endl;

    if(luckyNumber == winningNumber) {
        std::cout << "YOU WON!! CONGRATULATIONS!!" << std::endl;
    } else {
        std::cout << "You didn't win, but that was to be expected, wasn't it?" << std::endl;
    }
    
    return 0;
}